
jQuery('body').addClass('OptimizelyTest');
 
jQuery("#quick-delivery").remove();
jQuery("#quick-delivery-mobile").remove();
jQuery("#quick-delivery-popup").remove();
jQuery('<div id="quick-delivery" />').insertBefore(jQuery("#home_contact"));
 
jQuery("#quick-delivery").append('<div class="top" />');
jQuery("#quick-delivery .top").append('<img src="https://i.imgur.com/DcIHkOT.png" />');
jQuery("#quick-delivery .top").append('<p><strong>We produce, We deliver</strong>in just under 5 days</p>');
jQuery("#quick-delivery .top").append('<span class="question-icon"><i class="fa fa-question-circle" aria-hidden="true"></i></span>')
jQuery("#quick-delivery .top").append('<span class="arrow"><!-- --></span>')
 
jQuery("#quick-delivery").append('<div class="bottom" />');
jQuery("#quick-delivery .bottom").append('<ul><li><i class="fa fa-check" aria-hidden="true"></i> Choose your product</li><li><i class="fa fa-check" aria-hidden="true"></i> Customise with your logo</li><li><i class="fa fa-check" aria-hidden="true"></i> Choose the quantity</li><li><i class="fa fa-check" aria-hidden="true"></i> We create!</li></ul>');
     
jQuery("#quick-delivery").append('<div class="footer-text" />');
jQuery("#quick-delivery .footer-text").append('<p>High <strong>durability</strong> <span><i class="fa fa-circle" aria-hidden="true"></i></span> High <strong>quality</strong></p>');
 
jQuery("body").append('<div id="quick-delivery-popup"><div id="quick-delivery-popup-inner"><a href="#" class="popup-close js-close-popup"><i class="fa fa-times" aria-hidden="true"></i></a></div></div>');
 
jQuery("#quick-delivery-popup-inner").append('<div class="top" />');
jQuery("#quick-delivery-popup-inner .top").append('<div class="wrap"><img src="https://i.imgur.com/DcIHkOT.png" /><p><strong>We produce, We deliver</strong>in just under 5 days</p></div>');
 
jQuery("#quick-delivery-popup-inner").append('<div class="middle" />');
jQuery("#quick-delivery-popup-inner .middle").append('<div class="step step-1"><h2 class="title"><span><span>1</span> Choose your product</span></h2><img src="https://i.imgur.com/oddQWpj.jpg" /></div>');
jQuery("#quick-delivery-popup-inner .middle").append('<div class="step step-2"><h2 class="title"><span><span>2</span> Customize with your logo</span></h2><img src="https://i.imgur.com/cnI5d5O.jpg" /></div>');
jQuery("#quick-delivery-popup-inner .middle").append('<div class="step step-3"><h2 class="title"><span><span>3</span> Choose your quantity</span></h2><img src="https://i.imgur.com/qv71mPR.jpg" /></div>');
 
jQuery("#quick-delivery-popup-inner").append('<div class="bottom" />');
jQuery("#quick-delivery-popup-inner .bottom").append('<h2>We produce high quality, high durability products catered to your needs.</h2>');
jQuery("#quick-delivery-popup-inner .bottom").append('<a class="button" onclick="popupCenter(\'/email.php?product=Popup Enquiry Widget\', \'StatusBar\', 450, 700);  return false;" href="/email.php?product=Popup Enquiry Widget" target="_blank">ENQUIRE NOW</a>');
jQuery("#quick-delivery-popup-inner .bottom").append('<span>or</span><a href="#" class="link js-close-popup">Continue Shopping</a>');
 
 
jQuery('<div id="quick-delivery-mobile" />').insertAfter(jQuery(".header-top .search-bar-bg"));
jQuery("#quick-delivery-mobile").append('<img src="https://i.imgur.com/DcIHkOT.png" />');
jQuery("#quick-delivery-mobile").append('<p><strong>We produce, We deliver</strong>in just under 5 days</p>');
jQuery("#quick-delivery-mobile").append('<span class="question-icon"><i class="fa fa-question-circle" aria-hidden="true"></i></span>')
 
 
jQuery("#quick-delivery, #quick-delivery-mobile").click(function(e){
    e.preventDefault()
    jQuery("#quick-delivery-popup").show().animate({opacity:1});
})
 
jQuery(".js-close-popup").click(function(e){
    e.preventDefault()
    jQuery("#quick-delivery-popup").animate({opacity:0},300,function(){
        jQuery("#quick-delivery-popup").hide();
    });
})
     
     
jQuery('head').append(`
<style type="text/css">
    .OptimizelyTest #quick-delivery {margin-top:6px; cursor:pointer;}
    .OptimizelyTest #quick-delivery:hover {opacity:0.7;}
    .OptimizelyTest #quick-delivery .top {background-color: #028AD1; text-align:center; padding-top:15px; position:relative;}
    .OptimizelyTest #quick-delivery .top img {width:160px;}
    .OptimizelyTest #quick-delivery .top p {font-size:14px; line-height:16px; color:#fff; padding:10px 0 0px 0; margin-bottom:0px;}
    .OptimizelyTest #quick-delivery .top p strong {font-size:15px; color:#fff; display:block;}
    .OptimizelyTest #quick-delivery .top span.question-icon {color:#fff; font-size:20px; padding-bottom:16px;}
    .OptimizelyTest #quick-delivery .top span.arrow {position:absolute; left:50%; margin-left:-10px; bottom:-8px; width: 0; height: 0; border-style: solid; border-width: 8px 10px 0 10px; border-color: #008cd2 transparent transparent transparent;}
     
    .OptimizelyTest #quick-delivery .bottom {background-color: #199CE1;}
    .OptimizelyTest #quick-delivery .bottom ul {padding:15px 0 10px 0; margin:0;}
    .OptimizelyTest #quick-delivery .bottom ul li {color:#fff; font-size:14px; position:relative; padding-left:10px; line-height:17px;}
     
     
     
    .OptimizelyTest #quick-delivery .footer-text {background-color: #F5F5F5; border-top:1px solid #fff;}
    .OptimizelyTest #quick-delivery .footer-text p {font-size:14px; color:#000; text-align:center; line-height:32px;}
    .OptimizelyTest #quick-delivery .footer-text p strong {font-weight:normal; color:#72ad13;}
    .OptimizelyTest #quick-delivery .footer-text p i {font-size:5px; position:relative; top:-3px; padding:0 2px;}    
     
    .OptimizelyTest #quick-delivery-popup {width:100vw; height:100vh; display:none; opacity:0; z-index:1000; position:fixed; top:0; left:0; background:rgba(0,0,0,0.7);}
    .OptimizelyTest #quick-delivery-popup-inner {width:100%; max-width:730px; height:410px; position:absolute; top:96px; left:50%; margin-left:-375px; background-color:#fff;}
    .OptimizelyTest #quick-delivery-popup .popup-close {font-size:34px; color:#fff; position:absolute; right:-35px; top:-35px;}
     
    .OptimizelyTest #quick-delivery-popup .top {width:100%; height:52px; background-color:#028AD1; text-align:center;}
    .OptimizelyTest #quick-delivery-popup .top img {position: relative; top: -10px; margin-right:20px;}
    .OptimizelyTest #quick-delivery-popup .top p {display: inline-block; color: #fff; font-size: 14px; margin: 5px 0 0 0;}
    .OptimizelyTest #quick-delivery-popup .top p strong {font-size: 16px; display: block;}
     
    .OptimizelyTest #quick-delivery-popup .middle:after {content:""; display:block; clear:both;}
    .OptimizelyTest #quick-delivery-popup .middle {max-width:610px; margin:0 auto; padding:20px 0;}
    .OptimizelyTest #quick-delivery-popup .middle .step {width:33.33333%; float:left;}
    .OptimizelyTest #quick-delivery-popup .middle .step h2 {font-size:12px; color: #028AD1; line-height:24px;}
    .OptimizelyTest #quick-delivery-popup .middle .step h2 span {background-color:#fff; display:inline-block; padding-right:4px; position:relative; z-index:2;}
    .OptimizelyTest #quick-delivery-popup .middle .step h2 span span {color:#fff; background-color: #028AD1; width:24px; height:24px; display:block; float:left; margin-right:9px; border-radius:100%; text-align:center; line-height:24px; padding:0;}
    .OptimizelyTest #quick-delivery-popup .middle .step h2:after {content:""; display:block; width:98%; height:1px; background-color:#028AD1; position:relative; top:-12px;}
    .OptimizelyTest #quick-delivery-popup .middle .step-3 h2:after {display:none;}
     
    .OptimizelyTest #quick-delivery-popup .bottom {background-color: #F3F3F3; padding:20px 0; text-align:center}
    .OptimizelyTest #quick-delivery-popup .bottom h2 { font-size: 15px; color: #028AD1; font-weight:700;}
    .OptimizelyTest #quick-delivery-popup .bottom a.button {display:inline-block; width:150px; height:32px; border-bottom:5px solid #6aa000; background-color:#7ab10a; color:#fff; text-align:center; font-size:15px; font-weight:bold; text-transform:uppercase; line-height:32px; border-radius:3px; margin-bottom:6px;}
    .OptimizelyTest #quick-delivery-popup .bottom a.button:hover {background-color:#6aa000; }
    .OptimizelyTest #quick-delivery-popup .bottom span {display:block;}
    .OptimizelyTest #quick-delivery-popup .bottom a.link {text-decoration:underline !important; color:#000;}
    .OptimizelyTest #quick-delivery-popup .bottom a.link:hover {text-decoration:none!important;}
     
     
    .OptimizelyTest #quick-delivery-mobile {display:none;} 
     
     
    @media screen and (max-width:767px){
        .OptimizelyTest #quick-delivery {display:none;}
        .OptimizelyTest #quick-delivery-mobile {display:block; width:100%; height:52px; background-color:#028AD1; text-align:center; float:left;}
        .OptimizelyTest #quick-delivery-mobile img {position: relative; top: -10px; margin-right:20px;}
        .OptimizelyTest #quick-delivery-mobile p {display: inline-block; color: #fff; font-size: 14px; margin: 5px 0 0 0;}
        .OptimizelyTest #quick-delivery-mobile p strong {font-size: 16px; display: block;}
        .OptimizelyTest #quick-delivery-mobile span.question-icon {color:#fff; font-size:20px; float:right; margin-top:12px; margin-right:12px; padding-bottom:0;}  
         
        .OptimizelyTest #quick-delivery-popup-inner {width:96%; left:2%; margin-left:0;}
        .OptimizelyTest #quick-delivery-popup .popup-close {right:0px; top:-43px; font-size:30px;}
         
        .OptimizelyTest #quick-delivery-popup-inner .middle {max-width:100%;}
        .OptimizelyTest #quick-delivery-popup-inner .middle .step {width:100%; max-width:50%; margin:0 auto; text-align:center;}
        .OptimizelyTest #quick-delivery-popup-inner .middle .step-3 {float:none;}
        .OptimizelyTest #quick-delivery-popup-inner .middle .step h2 {margin-bottom:0;}
        .OptimizelyTest #quick-delivery-popup-inner .middle .step h2:after {content:""; display:none;}
        .OptimizelyTest #quick-delivery-popup-inner .middle .step h2 span span {display:block; margin:0 auto; float:none;}
        .OptimizelyTest #quick-delivery-popup-inner .middle .step img {max-width:100px;}
         
        .OptimizelyTest #quick-delivery-popup-inner .bottom {padding:10px 5px;}
         
         
       
    }
     
    @media screen and (max-width:370px){
        .OptimizelyTest #quick-delivery-mobile {text-align:left;}
        .OptimizelyTest #quick-delivery-mobile img {left:-17px; margin-right:0;}
        .OptimizelyTest #quick-delivery-mobile p {font-size:12px;}
        .OptimizelyTest #quick-delivery-mobile p strong {font-size:14px;}
         
        .OptimizelyTest #quick-delivery-popup .top img {max-width:80px; marin-right:10px;}
        .OptimizelyTest #quick-delivery-popup-inner .middle .step img {max-width:80px;}
         
        .OptimizelyTest #quick-delivery-popup-inner .bottom h2 {font-size:14px;}
        .OptimizelyTest #quick-delivery-popup-inner .middle {padding:10px 0;}
    }
     
</style>
`);